package ua.od.atomspace.twitter.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.od.atomspace.twitter.ResourceNotFoundException;
import ua.od.atomspace.twitter.dao.model.User;
import ua.od.atomspace.twitter.dao.repository.UserRepository;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/users")
public class UserController {

  private final UserRepository userRepository;

  @Autowired
  public UserController(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @ResponseBody
  @GetMapping
  @ResponseStatus(code = HttpStatus.OK)
  public Iterable<User> list() {
    return userRepository.findAll();
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  @ResponseBody
  public User create(@RequestBody User user) {
    return userRepository.save(user);
  }

  @GetMapping("/{id}")
  @ResponseBody
  @ResponseStatus(code = HttpStatus.OK)
  public User get(@PathVariable Long id) throws ResourceNotFoundException {
    return userRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
  }

  @PutMapping("/{id}")
  @ResponseBody
  @ResponseStatus(code = HttpStatus.OK)
  public User update(@PathVariable Long id, @RequestBody User user) throws ResourceNotFoundException {
    User response = userRepository.findById(id).orElseThrow(ResourceNotFoundException::new);

    response.setFirstName(user.getFirstName());
    response.setSecondName(user.getSecondName());
    response.setAge(user.getAge());
    response.setUsername(user.getUsername());
    response.setCreatedAt(user.getCreatedAt());

    response = userRepository.save(response);
    return response;
  }

  @GetMapping(params = { "ageGreaterThan" })
  @ResponseBody
  public List<User> listAgeGreaterThan(@RequestParam Integer ageGreaterThan) {
    List<User> response = userRepository.findAllByAgeGreaterThan(ageGreaterThan);
    log.info("Get by age greater than {}",ageGreaterThan);
    return response;
  }

}
