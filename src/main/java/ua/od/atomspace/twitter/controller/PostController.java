package ua.od.atomspace.twitter.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.od.atomspace.twitter.dao.model.Post;
import ua.od.atomspace.twitter.dao.repository.PostRepository;

@RestController
@Slf4j
@RequestMapping("/api/posts")
public class PostController {

  private final PostRepository postRepository;

  @Autowired
  public PostController(PostRepository postRepository) {
    this.postRepository = postRepository;
  }

  @ResponseBody
  @GetMapping
  @ResponseStatus(code = HttpStatus.OK)
  public Iterable<Post> list() {
    return postRepository.findAll();
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  @ResponseBody
  public Post create(@RequestBody Post post) {
    return postRepository.
        save(post);
  }
}
