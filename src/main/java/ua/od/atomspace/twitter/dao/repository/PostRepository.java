package ua.od.atomspace.twitter.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.od.atomspace.twitter.dao.model.Post;

public interface PostRepository extends CrudRepository<Post, Long> {
}
