package ua.od.atomspace.twitter.dao.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_id")
  private Long userId;

  @Column(name = "age")
  private Integer age;

  @Column(name = "first_name", length = 30)
  private String firstName;

  @Column(name = "second_name", length = 30)
  private String secondName;

  @Column(name = "username", length = 280)
  private String username;

  @Column(name = "password", length = 50)
  private String password;

  @Column(name = "created_at")
  private Date createdAt;
}
