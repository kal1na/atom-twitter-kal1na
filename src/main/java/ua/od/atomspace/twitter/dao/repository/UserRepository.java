package ua.od.atomspace.twitter.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.od.atomspace.twitter.dao.model.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
  List<User> findAllByAgeGreaterThan(Integer age);
}
